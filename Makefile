# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

VER:=$(shell sed -n -e \
         's/ *"version": "\([0-9]\(\.[0-9]\)\+\([a-z][0-9]\?\)\?\)",/\1/p' \
         manifest.json)
PKGDIR=jsactions_k_$(VER)

.PHONY: all package

all: ;

package:
	mkdir $(PKGDIR)
	mkdir $(PKGDIR)/icons
	cp -t $(PKGDIR) COPYING manifest.json samples.json \
	  content.js jsacontent.js jsacscript.js event.js jsaevent.js \
	  options.html options.js actnsed.js
	cp -t $(PKGDIR)/icons icons/*.png
	cd $(PKGDIR) && zip -9 -r ../jsactions_k_$(VER).xpi *
	rm -r $(PKGDIR)
