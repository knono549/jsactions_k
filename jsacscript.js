/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict';

function JsaCScript(aEvent, aContext, aInfo) {
    this.event = new JsaCScript.Event(aEvent);
    this.context = new JsaCScript.Context(aContext);
    this.info = new JsaCScript.Info(aInfo);
    this.tabs = new JsaCScript.Tabs();
    this.windows = new JsaCScript.Windows();
    this.clipboard = new JsaCScript.Clipboard();
    this.runtime = new JsaCScript.Runtime();
    // for portability
    this.pref = new JsaCScript.Pref();
}
JsaCScript.prototype = {
  // for compatibility
  get clickButton() {
      console.info("JsaCScript: clickButton is deprecated." +
                     "Please use event.button instead.");
      return this.event.button;
  },

  get withKey() {
      console.info("JsaCScript: withKey is deprecated." +
                     "Please use event.*Key instead.");
      var withKey = 0;
      var event = this.event;
      if (event.shiftKey) { withKey += 1; }
      if (event.ctrlKey) { withKey += 2; }
      if (event.altKey) { withKey += 4; }
      if (event.metaKey) { withKey += 8; }
      return withKey;
  },

  dump: function (aMsg) {
      console.info("JsaCScript: dump is deprecated." +
                     "Please use window.console.log instead.");
      console.log(aMsg);
      return true;
  },

  jsaMsg: function (aMsg) {
      console.info("JsaCScript: jsaMsg is deprecated." +
                     "Please use window.console.log instead.");
      console.log(aMsg);
      return true;
  },

  jsaErr: function (aError, aURI) {
      console.info("JsaCScript: jsaErr is deprecated." +
                     "Please use window.console.error instead.");
      console.error(aError.toString());
      return true;
  },

  setClipBoardString: function (aString) {
      console.info("JsaCScript: setClipBoardString is deprecated." +
                     "Please use clipboard.setString instead.");
      var textarea = document.createElement("textarea");
      document.body.appendChild(textarea);
      textarea.textContent = aString;
      textarea.select();
      document.execCommand("Copy");
      textarea.parentNode.removeChild(textarea);
  },

  getClipBoardString: function () {
      console.info("JsaCScript: getClipBoardString is deprecated." +
                     "Please use clipboard.getString instead.");
      var textarea = document.createElement("textarea");
      textarea.contentEditable = "true";
      textarea.style.setProperty("position", "fixed", "important");
      textarea.style.setProperty("top", "0", "important");
      textarea.style.setProperty("left", "0", "important");
      textarea.style.setProperty("opacity", "0", "important");
      document.body.appendChild(textarea);
      textarea.focus();
      document.execCommand("Paste");
      var text = textarea.textContent;
      window.setTimeout(function () {
          textarea.parentNode.removeChild(textarea);
      }, 10);
      return text;
  },

  // for portability
  get currentScriptPath() {
      throw new Error(
          "JsaCScript: currentScriptPath is no longer supported.");
  },

  get currentScriptURI() {
      throw new Error(
          "JsaCScript: currentScriptURI is no longer supported.");
  },

  loadURL: function (aURI, aReferrerURI) {
      throw new Error("JsaCScript: loadURL is no longer supported." +
                        "Please use tabs.update instead.");
  },

  addTab: function (aURI, aReferrerURI) {
      throw new Error("JsaCScript: addTab is no longer supported." +
                        "Please use tabs.create instead.");
  },

  addFGTab: function (aURI, aReferrerURI) {
      throw new Error("JsaCScript: addFGTab is no longer supported." +
                        "Please use tabs.create instead.");
  },

  exec: function (aPath, aArgs, aBlocking) {
      throw new Error("JsaCScript: exec is no longer supported." +
                        "Please use runtime.callProcess instead.");
  },

  convertURIToFilePath: function (aURI) {
      throw new Error(
          "JsaCScript: convertURIToFilePath is no longer supported.");
  },

  jsaEval: function (aString) {
      throw new Error("JsaCScript: jsaEval is no longer supported.");
  },

  jsaeval: function (aString) {
      throw new Error("JsaCScript: jsaeval is no longer supported.");
  },

  jsaAlert: function (aText) {
      throw new Error("JsaCScript: jsaAlert is no longer supported." +
                        "Please use window.Notification instead.");
  },

  popAutoHideAlert: function (aText) {
      throw new Error("JsaCScript: popAutoHideAlert is no longer supported." +
                        "Please use window.Notification instead.");
  },

  convertCharCodeFrom: function (aString, aCharset) {
      throw new Error(
          "JsaCScript: convertCharCodeFrom is no longer supported.");
  },

  convertCharCodeTo: function (aString, aCharset) {
      throw new Error(
          "JsaCScript: convertCharCodeTo is no longer supported.");
  },

  fileRead: function (aPath) {
      throw new Error("JsaCScript: fileRead is no longer supported.");
  },
};

JsaCScript.Event = function (aEvent) {
    this.button = aEvent.button;
    this.shiftKey = aEvent.shiftKey;
    this.ctrlKey = aEvent.ctrlKey;
    this.altKey = aEvent.altKey;
    this.metaKey = aEvent.metaKey;
};

JsaCScript.Context = function (aContext) {
    var target = aContext.target;
    var link = null;
    if (aContext.onLink) {
        var node = target;
        while (node) {
            if (node.localName == "a") { link = node; break; }
            node = node.parentNode;
        }
    }
    this.target = target;
    this.link = link;
    this.onLink = aContext.onLink;
    this.onImage = aContext.onImage;
    this.onAudio = aContext.onAudio;
    this.onVideo = aContext.onVideo;
    this.isTextSelected = aContext.isTextSelected;
    this.textSelected = aContext.textSelected;
};
JsaCScript.Context.prototype = {
  // for Compatibility
  get selection() {
      console.info("JsaCScript: context.selection is deprecated." +
                     "Please use context.textSelected instead.");
      return this.textSelected;
  },

  get location() {
      console.info("JsaCScript: context.location is deprecated." +
                     "Please use window.location.href instead.");
      return this.target.ownerDocument.location.href;
  }
};

JsaCScript.Info = function (aInfo) {
    this.scriptTitle = aInfo.scriptTitle;
    this.scriptReqs = aInfo.scriptReqs.slice();
    this.scriptSource = aInfo.scriptSource;
    this.scriptPath = aInfo.scriptPath;
};

JsaCScript.Tabs = function () {
};
JsaCScript.Tabs.prototype = {
  create: function (aData) {
      var data = {};
      if (typeof aData == "object") {
          for (var m in aData) {
              switch (m) {
                case "windowId":
                case "url":
                case "active":
                case "pinned":
                case "jsaposition": //
                  data[m] = aData[m];
                  break;
              }
          }
      }
      return new Promise(function (aResolve, aReject) {
          chrome.runtime.sendMessage(
              {name: "jsacscript.tabs.create", params: {data: data}},
              function (aResp) {
                  var e;
                  var le = chrome.runtime.lastError;
                  if (le) {
                      e = new Error("Could not create a tab.");
                      e.name = "JsaCScriptError";
                      aReject(e);
                      return;
                  }
                  var error = aResp.error;
                  if (error) {
                      e = new Error(error.message);
                      e.name = "JsaCScriptError";
                      aReject(e);
                      return;
                  }
                  aResolve(aResp.tab);
              });
      });
  },

  update: function (aTabId, aData) {
      if (typeof aTabId == "object") { aData = aTabId; aTabId = void(0); }
      var data = {};
      if (typeof aData == "object") {
          for (var m in aData) {
              switch (m) {
                case "url":
                case "active":
                case "pinned":
                case "muted":
                  data[m] = aData[m];
                  break;
              }
          }
      }
      return new Promise(function (aResolve, aReject) {
          chrome.runtime.sendMessage(
              {
                name: "jsacscript.tabs.update",
                params: {tabId: aTabId, data: data}
              },
              function (aResp) {
                  var e;
                  var le = chrome.runtime.lastError;
                  if (le) {
                      e = new Error("Could not update a tab.");
                      e.name = "JsaCScriptError";
                      aReject(e);
                      return;
                  }
                  var error = aResp.error;
                  if (error) {
                      e = new Error(error.message);
                      e.name = "JsaCScriptError";
                      aReject(e);
                      return;
                  }
                  aResolve(aResp.tab);
              });
      });
  }
};

JsaCScript.Windows = function () {
};
JsaCScript.Windows.prototype = {
  create: function (aData, aCallback) {
      var data = {};
      if (typeof aData == "object") {
          for (var m in aData) {
              switch (m) {
                case "url":
                case "tabId":
                case "focused":
                case "incognito":
                  data[m] = aData[m];
                  break;
              }
          }
      }
      return new Promise(function (aResolve, aReject) {
          chrome.runtime.sendMessage(
              {name: "jsacscript.windows.create", params: {data: data}},
              function (aResp) {
                  var le = chrome.runtime.lastError;
                  var e;
                  if (le) {
                      e = new Error("Could not create a window.");
                      e.name = "JsaCScriptError";
                      aReject(e);
                  }
                  var error = aResp.error;
                  if (error) {
                      e = new Error(error.message);
                      e.name = "JsaCScriptError";
                      aReject(e);
                  }
                  aResolve(aResp.window);
              });
      });
  }
};

JsaCScript.Clipboard = function () {
};
JsaCScript.Clipboard.prototype = {
  setString: function (aString) {
      return new Promise(function (aResolve, aReject) {
          var textarea = document.createElement("textarea");
          document.body.appendChild(textarea);
          textarea.textContent = aString;
          textarea.select();
          document.execCommand("Copy");
          textarea.parentNode.removeChild(textarea);
          aResolve();
      });
  },

  getString: function () {
      return new Promise(function (aResolve, aReject) {
          var textarea = document.createElement("textarea");
          textarea.contentEditable = "true";
          textarea.style.setProperty("position", "fixed", "important");
          textarea.style.setProperty("top", "0", "important");
          textarea.style.setProperty("left", "0", "important");
          textarea.style.setProperty("opacity", "0", "important");
          document.body.appendChild(textarea);
          textarea.focus();
          document.execCommand("Paste");
          var text = textarea.textContent;
          window.setTimeout(function () {
              textarea.parentNode.removeChild(textarea);
          }, 10);
          aResolve(text);
      });
  }
};

JsaCScript.Runtime = function () {
};
JsaCScript.Runtime.prototype = {
  callProcess: function (aData) {
      var data = {};
      if (typeof aData == "object") {
          for (var m in aData) {
              switch (m) {
                case "params":
                  data[m] = typeof aData["params"] == "string" ?
                    [aData["params"]] : aData["params"];
                  break;
                case "file":
                case "directory":
                case "communicate":
                case "input":
                case "inencoding":
                case "outencoding":
                case "errencoding":
                  data[m] = aData[m];
                  break;
              }
          }
      }
      return new Promise(function (aResolve, aReject) {
          chrome.runtime.sendMessage(
              {name: "jsacscript.runtime.callprocess", params: {data: data}},
              function (aResp) {
                  var e;
                  var le = chrome.runtime.lastError;
                  if (le) {
                      e = new Error("Could not call a process.");
                      e.name = "JsaCScriptError";
                      aReject(e);
                      return;
                  }
                  var error = aResp.error;
                  if (error) {
                      e = new Error(error.message);
                      e.name = "JsaCScriptError";
                      aReject(e);
                      return;
                  }
                  aResolve(aResp.result);
              });
      });
  }
};

JsaCScript.Pref = function () {
};
JsaCScript.Pref.prototype = {
  // for portability
  setBool: function (aPrefName, aValue) {
      throw new Error("JsaCScript: pref.setBool is no longer supported.");
  },
  getBool: function (aPrefName, aDefault) {
      throw new Error("JsaCScript: pref.getBool is no longer supported.");
  },
  setInt: function (aPrefName, aValue) {
      throw new Error("JsaCScript: pref.setInt is no longer supported.");
  },
  getInt: function (aPrefName, aDefault) {
      throw new Error("JsaCScript: pref.getInt is no longer supported.");
  },
  setUniChar: function (aPrefName, aValue) {
      throw new Error("JsaCScript: pref.setUniChar is no longer supported.");
  },
  getUniChar: function (aPrefName, aDefault) {
      throw new Error("JsaCScript: pref.getUniChar is no longer supported.");
  },
  copyUniChar: function (aPrefName, aDefault) {
      throw new Error("JsaCScript: pref.copyUniChar is no longer supported.");
  },
  setLocalizedUniChar: function (aPrefName, aValue) {
      throw new Error(
          "JsaCScript: pref.setLocalizedUniChar is no longer supported.");
  },
  getLocalizedUniChar: function (aPrefName, aDefault) {
      throw new Error(
          "JsaCScript: pref.getLocalizedUniChar is no longer supported.");
  }
};
