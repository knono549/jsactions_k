/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict';

var menuTree = {
  /*
   * {
   *   type: "jsamenu",
   *   contexts: ["page", "selection", "link", "image", "video", "audio"],
   *   title: "JsActions",
   *   children: [
   *       ...
   *   ]
   * }
   */
  _tree: null,

  /*
   * path <=> node
   */
  _pathNodeMap: null,
  /*
   * node <=> path
   */
  _nodePathMap: null,

  /*
   * menuitem id <=> node
   * {
   *   jsa_jsamenu_0: {}, //reffer to the first "jsamenu" type node
   *   jsa_normal_0: {} //reffer to the first "normal" type node
   *        ...
   * }
   */
  _mIdNodeMap: null,

  /*
   * {
   *   name: "Firefox",
   *   vendor: "Mozilla",
   *   version: "xx.x",
   *   buildId: "xxxxxxxxxxxxxx"
   * }
   */
  _browserInfo: null,

  _createXHR: function () {
      return new XMLHttpRequest();
  },

  _importSamples: function () {
      return new Promise((function (aResolve, aReject) {
          var url = chrome.extension.getURL("samples.json");
          var request = this._createXHR();
          var cb = function (aEvent) {
              switch (aEvent.type) {
                case "error":
                  aReject(new Error("Could not read file."));
                  return;
                  break;
                case "load":
                  var data;
                  try {
                      data = JSON.parse(aEvent.target.responseText);
                  } catch (e) {
                      aReject(new Error("Could not parse file."));
                      return;
                  }
                  var tree = data.tree;
                  var items = {tree: tree};
                  var script = data.script;
                  var ids = [];
                  for (var id in script) {
                      ids.push(id);
                      items["script." + id] = script[id];
                  }
                  items["scriptIds"] = ids;
                  chrome.storage.local.set(items, function () {
                      var e = chrome.runtime.lastError;
                      if (e) { aReject(e); return; }
                      aResolve(data);
                  });
                  break;
              }
          };
          request.addEventListener("error", cb, false);
          request.addEventListener("load", cb, false);
          request.open("GET", url);
          request.send();
      }).bind(this));
  },

  _getTree: function () {
      return new Promise((function (aResolve, aReject) {
          if (this._tree) {
              aResolve(this._tree);
              return;
          }
          var key = "tree";
          chrome.storage.local.get([key], (function (aItems) {
              var e = chrome.runtime.lastError;
              if (e) { aReject(e); return; }

              var tree = aItems[key];
              if (tree) {
                  this._tree = tree;
                  aResolve(this._tree);
                  return;
              }

              this._importSamples().then((function (aData) {
                  this._tree = aData.tree;
                  aResolve(this._tree);
              }).bind(this)).catch(function (aE) {
                  aReject(aE);
              });
          }).bind(this));
      }).bind(this));
  },

  init: function (aRebuildMenuitems) {
      if(typeof aRebuildMenuitems == "undefined") { aRebuildMenuitems = true; }

      this._tree = null;
      this._pathNodeMap = null;
      this._nodePathMap = null;
      this._mIdNodeMap = null;
      this._browserInfo = null;

      chrome.contextMenus.onClicked.
        addListener(this.onClicked.bind(this));
      chrome.storage.onChanged.
        addListener(this.onChanged.bind(this));

      return new Promise((function (aResolve, aReject) {
          if (!aRebuildMenuitems) {
              aResolve();
              return;
          }
          this._getTree().then((function (aTree) {
              this.rebuildMenuitems(aTree).then((function (aMIdNodeMap) {
                  this._mIdNodeMap = aMIdNodeMap;
                  aResolve();
              }).bind(this));
          }).bind(this)).catch(function (aE) {
              aReject(aE);
          });
      }).bind(this));
  },

  normalizePath: function (aPath, aBase) {
      var psegs = aPath.split("/");
      if (!aBase || psegs[0] == "") { aBase = "/"; }
      var bsegs = aBase.split("/");
      var seg = bsegs[bsegs.length - 1];
      if (!(seg == "." || seg == "..")) { bsegs.pop(); }

      var nsegs = [];
      [bsegs, psegs].forEach(function (aSegs, aIndex, aArray) {
          var seg;
          for (var i = 0, l = aSegs.length; i < l; ++i) {
              seg = aSegs[i];
              if (seg == "" &&
                  !((aIndex == 0 && i == 0) ||
                    (aIndex == aArray.length - 1 && i == l - 1))) {
                  continue;
              }
              if (seg == ".") {
                  if (nsegs.length == 0 &&
                      bsegs.length > 0 &&
                      bsegs[0] == "") { nsegs.push(""); }
                  continue;
              }
              if (seg == "..") {
                  if (nsegs.length > 0) { nsegs.pop(); }
                  if (nsegs.length == 0 &&
                      bsegs.length > 0 &&
                      bsegs[0] == "") { nsegs.push(""); }
                  continue;
              }
              nsegs.push(seg);
          }
      });

      return nsegs.join("/");
  },

  _getPathNodeMaps: function (){
      return new Promise((function (aResolve, aReject) {
          if (this._pathNodeMap && this._nodePathMap) {
              aResolve({
                pathNodeMap: this._pathNodeMap,
                nodePathMap: this._nodePathMap
              });
              return;
          }
          this._getTree().then((function (aTree) {
              var pathNodeMaps = this._createPathNodeMaps(aTree);
              this._pathNodeMap = pathNodeMaps.pathNodeMap;
              this._nodePathMap = pathNodeMaps.nodePathMap;
              aResolve(pathNodeMaps);
          }).bind(this)).catch(function (aE) {
              aReject(aE);
          });
      }).bind(this));
  },

  _getPathNodeMap: function () {
      return new Promise((function (aResolve, aReject) {
          this._getPathNodeMaps().then(function (aPathNodeMaps) {
              aResolve(aPathNodeMaps.pathNodeMap);
          }).catch(function (aE) {
              aReject(aE);
          });
      }).bind(this));
  },

  _getNodePathMap: function () {
      return new Promise((function (aResolve, aReject) {
          this._getPathNodeMaps().then(function (aPathNodeMaps) {
              aResolve(aPathNodeMaps.nodePathMap);
          }).catch(function (aE) {
              aReject(aE);
          });
      }).bind(this));
  },

  _createPathNodeMaps: function (aTree) {
      function t2pnmr(aNode, aPathNodeMap, aNodePathMap, aPath) {
          if (!("title" in aNode)) { return; }
          aPath = aPath ? aPath + aNode.title : "";
          if (!aPathNodeMap.get(aPath)) { aPathNodeMap.set(aPath, aNode); }
          aNodePathMap.set(aNode, aPath);

          var nodes = aNode.children;
          if (!nodes) { return; }

          for (var i = 0, l = nodes.length; i < l; ++i) {
              t2pnmr(nodes[i], aPathNodeMap, aNodePathMap, aPath + "/");
          }
      }

      var pathNodeMap = new Map();
      var nodePathMap = new Map();
      t2pnmr(aTree, pathNodeMap, nodePathMap);
      return {pathNodeMap: pathNodeMap, nodePathMap: nodePathMap}
  },

  _getMIdNodeMap: function () {
      return new Promise((function (aResolve, aReject) {
          if (this._mIdNodeMap) {
              aResolve(this._mIdNodeMap);
              return;
          }
          this._getTree().then((function (aTree) {
              this.rebuildMenuitems(aTree).then((function (aMIdNodeMap) {
                  this._mIdNodeMap = aMIdNodeMap;
                  aResolve(this._mIdNodeMap);
              }).bind(this));
          }).bind(this)).catch(function (aE) {
              aReject(aE);
          });
      }).bind(this));
  },

  getNodeByMId: function (aMId) {
      return new Promise((function (aResolve, aReject) {
          this._getMIdNodeMap().then(function (aMIdNodeMap) {
              var node = aMIdNodeMap.get(aMId);
              aResolve(node ? node : null);
          }).catch(function (aE) {
              aReject(aE);
          });
      }).bind(this));
  },

  _getBrowserInfo: function () {
      return new Promise((function (aResolve, aReject) {
          if (this._browserInfo) {
              aResolve(this._browserInfo);
              return;
          }
          new Promise(function (aResolve, aReject) {
              if (!("getBrowserInfo" in chrome.runtime)) {
                  aResolve({
                    name: "",
                    vendor: "",
                    version: "",
                    buildId: ""
                  });
                  return;
              }
              chrome.runtime.getBrowserInfo(function (aBrowserInfo) {
                  aResolve(aBrowserInfo);
              });
          }).then((function (aBrowserInfo) {
              this._browserInfo = aBrowserInfo;
              aResolve(this._browserInfo);
          }).bind(this));
      }).bind(this));
  },

  rebuildMenuitems: function (aTree) {
      function buildr(aUseIcons, aCount, aNode, aMap, aParentId) {
          var ctxs = aNode.contexts;
          if (ctxs.length == 0) { return; }

          // TODO: We need the parameter which sets the access key.
          // TODO: We need the function which hide separators at both ends of
          //       a menu.
          var params = {};
          switch (aNode.type) {
            case "jsamenu":
              params["type"] = "normal";
              params["id"] = "jsa_jsamenu_" + aCount.jsamenu++;
              params["title"] = aNode.title;
              if (aParentId && aUseIcons) {
                  params["icons"] = {
                      "16": "icons/jsamenu-16x16.png",
                      "32": "icons/jsamenu-32x32.png"
                  };
              }
              break;
            case "normal":
              params["type"] = "normal";
              params["id"] = "jsa_normal_" + aCount.normal++;
              params["title"] = aNode.title;
              if (aParentId && aUseIcons) {
                  params["icons"] = {
                      "16": "icons/normal-16x16.png",
                      "32": "icons/normal-32x32.png"
                  };
              }
              break;
            case "separator":
              params["type"] = "separator";
              params["id"] = "jsa_separator_" + aCount.separator++;
              break;
            default:
              console.error("Unknown node type: " + aNode.type);
              return;
          }
          params["contexts"] = ctxs;
          if (aParentId) { params["parentId"] = aParentId; }

          var id = chrome.contextMenus.create(params, function () {
              var le = chrome.runtime.lastError;
              if (le) { console.error(le.toString()); }
          });
          aMap.set(id, aNode);

          var nodes = aNode.children;
          if (! nodes) { return; }
          for (var i = 0, l = nodes.length; i < l; ++i) {
              buildr(aUseIcons, aCount, nodes[i], aMap, id);
          }
      }

      return new Promise((function (aResolve, aReject) {
          chrome.contextMenus.removeAll((function () {
              this._getBrowserInfo().then((function (aBrowserInfo) {
                  var useIcons = aBrowserInfo.name == "Firefox";
                  var count =  {
                    jsamenu: 0,
                    normal: 0,
                    separator: 0
                  };
                  var mIdNodeMap = new Map();
                  buildr(useIcons, count, aTree, mIdNodeMap);
                  aResolve(mIdNodeMap);
              }).bind(this));
          }).bind(this));
      }).bind(this));
  },

  getFlatDeps: function (aNode) {
      function pkr(aMenuTree, aPathNodeMap,
                   aNode, aPath, aPaths, aKeys, alPaths) {
          if (!(alPaths instanceof Set)) { alPaths = new Set(); }

          if (alPaths.has(aPath)) { return; }
          alPaths.add(aPath);

          if (aNode && ("scriptReqs" in aNode)) {
              var reqs = aNode.scriptReqs;
              var rpath;
              var rnode;
              for (var i = 0, l = reqs.length; i < l; ++i) {
                  rpath = aMenuTree.normalizePath(reqs[i], aPath);
                  rnode = aPathNodeMap.get(rpath);
                  rnode = rnode ? rnode : null;
                  pkr(aMenuTree, aPathNodeMap,
                      rnode, rpath, aPaths, aKeys, alPaths);
              }
          }

          aPaths.push(aPath);
          if (aNode && ("scriptId" in aNode)) {
              aKeys.push("script." + aNode.scriptId);
          } else {
              aKeys.push(void(0));
          }
      }

      return new Promise((function (aResolve, aReject) {
          Promise.all([this._getPathNodeMap(), this._getNodePathMap()]).
            then((function (aValues) {
                var pathNodeMap = aValues[0];
                var nodePathMap = aValues[1];
                var path = nodePathMap.get(aNode);
                if (typeof path == "undefined") {
                    aReject(new Error("Could not get the path."));
                }
                var paths = [];
                var keys = [];
                pkr(this, pathNodeMap, aNode, path, paths, keys);

                var fkeys = keys.filter(function (aKey) { return !!aKey; });
                chrome.storage.local.get(fkeys, function (aItems) {
                    var le = chrome.runtime.lastError;
                    if (le) {
                        aReject(le);
                        return;
                    }

                    var deps = new Array(paths.length);
                    keys.forEach(function (aKey, aIndex) {
                        deps[aIndex] = {
                          path: paths[aIndex],
                          script: aKey ? aItems[aKey] : void(0)
                        };
                    });
                    aResolve(deps);
                });
            }).bind(this)).catch(function (aE) {
                aReject(aE);
            });
      }).bind(this));
  },

  _createInfoMessage: function (aInfo) {
      return new Promise((function (aResolve, aReject) {
          // TODO: We need the property like MouseEvent#button in aInfo.
          var button = 0;
          var shiftKey = false;
          var ctrlKey = false;
          var altKey = false;
          var metaKey = false;
          if ("modifiers" in aInfo) {
              aInfo.modifiers.forEach(function (aMod) {
                  switch (aMod) {
                    case "Shift": shiftKey = true; break;
                    case "Ctrl":
                    case "MacCtrl": ctrlKey = true; break;
                    case "Command": metaKey = true; break;
                  }
              });
          }

          var onLink = !!aInfo.linkUrl;
          var onImage = false;
          var onAudio = false;
          var onVideo = false;
          if ("mediaType" in aInfo) {
              switch (aInfo.mediaType) {
                case "image": onImage = true; break;
                case "audio": onAudio = true; break;
                case "video": onVideo = true; break;
              }
          }
          var isTextSelected = "selectionText" in aInfo;
          var textSelected = isTextSelected ? aInfo.selectionText : "";

          var mId = aInfo.menuItemId;
          this.getNodeByMId(mId).then((function (aNode) {
              this.getFlatDeps(aNode).then(function (aDeps) {
                  var paths = new Array(aDeps.length);
                  var scripts = new Array(aDeps.length);
                  aDeps.forEach(function (aDep, aIndex) {
                      paths[aIndex] = aDep.path;
                      scripts[aIndex] = aDep.script;
                  });

                  aResolve({
                    event: {
                      button: button,
                      shiftKey: shiftKey,
                      ctrlKey: ctrlKey,
                      altKey: altKey,
                      metaKey: metaKey
                    },
                    context: {
                      target: null,
                      onLink: onLink,
                      onImage: onImage,
                      onAudio: onAudio,
                      onVideo: onVideo,
                      textSelected: textSelected,
                      isTextSelected: isTextSelected
                    },
                    node: aNode,
                    paths: paths,
                    scripts: scripts
                  });
              }).catch(function (aE) {
                  aReject(aE);
              });
          }).bind(this)).catch(function (aE) {
              aReject(aE);
          });
      }).bind(this));
  },

  _sendMessageToActiveTab: function (aMsg) {
      return new Promise(function (aResolve, aReject) {
          var tabs = chrome.tabs;
          tabs.query({active: true, currentWindow: true}, function (aTabs) {
              if (aTabs.length == 0) {
                  aReject(new Error("Active tab is not found."));
                  return;
              }
              var tab = aTabs[0];

              tabs.sendMessage(
                  tab.id,
                  aMsg,
                  null,
                  function (aResponse) {
                      var le = chrome.runtime.lastError;
                      if (le) {
                          console.log("JSActions does not work on some " +
                                      "pages (e.g. addons.mozilla.org).");
                          aReject(le);
                          return;
                      }
                      aResolve();
                  });
          });
      });
  },

  onClicked: function (aInfo, aTab) {
      this._createInfoMessage(aInfo).then((function (aMsg) {
          this._sendMessageToActiveTab(aMsg).then(function () {
          }).catch(function (aE) {
              console.error(aE.toString());
          });
      }).bind(this)).catch(function (aE) {
          console.error(aE.toString());
      });
  },

  onChanged: function (aChanges, aAreaName) {
      if (aAreaName != "local") { return; }
      if (!("tree" in aChanges)) { return; }
      this._tree = aChanges["tree"].newValue;
      var pathNodeMaps = this._createPathNodeMaps(this._tree);
      this._pathNodeMap = pathNodeMaps.pathNodeMap;
      this._nodePathMap = pathNodeMaps.nodePathMap;
      this.rebuildMenuitems(this._tree).then((function (aMIdNodeMap) {
          this._mIdNodeMap = aMIdNodeMap;
      }).bind(this));
  }
};

var jsacsAgent = {
  caseTabsCreate: function (aMessage, aSender, aSendResponse) {
      var mparams = aMessage.params;
      var mdata = mparams.data;
      var tab = aSender.tab;
      var data = {};
      for (var m in mdata) {
          switch (m) {
            case "jsaposition":
              // TODO: We need the parameter which decides the index of the
              //       new tab automatically like relatedToCurrent of
              //       tabbrowser#addTab
              switch (mdata[m]) {
                case "last": break;
                case "first": data["index"] = 0; break;
                case "afterCurrent": data["index"] = tab.index + 1; break;
                case "beforeCurrent": data["index"] = tab.index; break;
                default:
                  aSendResponse(
                      {
                        error: {
                          name: "JsaListenerError",
                          message: "Invalid parameter is specified: " +
                                     "position => " + mdata[m]
                        }
                      });
                  return;
              }
              break;
            default:
              data[m] = mdata[m];
          }
      }
      try {
          chrome.tabs.create(data, function (aTab) {
              var le = chrome.runtime.lastError;
              if (le) {
                  console.error(le.toString());
                  aSendResponse(
                      {
                        error: {
                          name: "JsaListenerError",
                          message: "Could not create a tab."
                        }
                      });
                  return;
              }
              aSendResponse({tab: {id: aTab.id}});
          });
      } catch (e) {
          console.error(e.toString());
          aSendResponse(
              {
                error: {
                  name: "JsaListenerError",
                  message: "Could not create a tab."
                }
              });
      }
      return true;
  },

  caseTabsUpdate: function (aMessage, aSender, aSendResponse) {
      var mparams = aMessage.params;
      var tabId = mparams.tabId;
      var data = mparams.data;
      try {
          chrome.tabs.update(tabId, data, function (aTab) {
              var le = chrome.runtime.lastError;
              if (le) {
                  console.error(le.toString());
                  aSendResponse(
                      {
                        error: {
                          name: "JsaListenerError",
                          message: "Could not update a tab."
                        }
                      });
                  return;
              }
              aSendResponse({tab: {id: aTab.id}});
          });
      } catch (e) {
          console.error(e.toString());
          aSendResponse(
              {
                error: {
                  name: "JsaListenerError",
                  message: "Could not update a tab."
                }
              });
      }
      return true;
  },

  caseWindowsCreate: function (aMessage, aSender, aSendResponse) {
      var mparams = aMessage.params;
      var data = mparams.data;
      try {
          chrome.windows.create(data, function (aWindow) {
              var le = chrome.runtime.lastError;
              if (le) {
                  console.error(le.toString());
                  aSendResponse(
                      {
                        error: {
                          name: "JsaListenerError",
                          message: "Could not create a window."
                        }
                      });
                  return;
              }
              aSendResponse({window: {id: aWindow.id}});
          });
      } catch (e) {
          console.error(e.toString());
          aSendResponse(
              {
                error: {
                  name: "JsaListenerError",
                  message: "Could not create a window."
                }
              });
      }
      return true;
  },

  caseRuntimeCallProcess: function (aMessage, aSender, aSendResponse) {
      var mparams = aMessage.params;
      var data = mparams.data;
      chrome.runtime.sendNativeMessage("jsacprochost", data, function (aResp) {
          var le = chrome.runtime.lastError;
          if (le) {
              console.error(le.toString());
              aSendResponse(
                  {
                    error: {
                      name: "JsaListenerError",
                      message: "Could not call a process."
                    }
                  });
              return;
          }
          aSendResponse(aResp);
      });
      return true;
  },

  onMessage: function (aMessage, aSender, aSendResponse) {
      var mname = aMessage.name;
      if (!mname) { return false; }
      var ret = false;
      switch (mname) {
        case "jsacscript.tabs.create":
          ret = this.caseTabsCreate(aMessage, aSender, aSendResponse);
          break;
        case "jsacscript.tabs.update":
          ret = this.caseTabsUpdate(aMessage, aSender, aSendResponse);
          break;
        case "jsacscript.windows.create":
          ret = this.caseWindowsCreate(aMessage, aSender, aSendResponse);
          break;
        case "jsacscript.runtime.callprocess":
          ret = this.caseRuntimeCallProcess(aMessage, aSender, aSendResponse);
          break;
      }
      return ret;
  }
};
