INTRODUCTION
============

The extension for Firefox which adds "JSActions" to the content area
context menu.


REQUIREMENTS
------------

* GNU Make 4.2.1
  https://www.gnu.org/software/make/
* GNU sed 4.4
  https://www.gnu.org/software/sed/
* Zip 3.0
  http://www.info-zip.org/


INSTALLATION
------------

    $ make package
