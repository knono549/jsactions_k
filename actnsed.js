/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict';

var Util = {
  inheritPrototype: function (aSub, aSuper) {
      var prot = Object.create(aSuper);
      var dscs = {};
      Object.keys(aSub).forEach(function (aM) {
          dscs[aM] = Object.getOwnPropertyDescriptor(aSub, aM);
      });
      Object.defineProperties(prot, dscs);
      return prot;
  }
};

function Model() {
    this._observers = new Set();
}
Model.prototype = {
  addObserver: function (aObserver) {
      this._observers.add(aObserver);
  },

  removeObserver: function (aObserver) {
      this._observers.delete(aObserver);
  },

  notifyObservers: function (aTopic, aData) {
      this._observers.forEach(function (aObserver) {
          aObserver.observe(this, aTopic, aData);
      }, this);
  }
};

function ViewController() {
    this._model = null;
}
ViewController.prototype = {
  setModel: function (aModel) {
      if (this._model) {
          this._model.removeObserver(this);
          this._model = null;
      }
      aModel.addObserver(this);
      this._model = aModel;
  },

  observe: function (aSubject, aTopic, aData) {
  }
};

function ProxyM(aDialogsM, aTreeM, aScriptsM) {
    this._models = [aDialogsM, aTreeM, aScriptsM].
      filter(function (aModel) { return !!aModel; });
    this.dialogs = aDialogsM ? aDialogsM : null;
    this.tree = aTreeM ? aTreeM : null;
    this.scripts = aScriptsM ? aScriptsM : null;
}
ProxyM.prototype = {
  addObserver: function (aObserver) {
      this._models.forEach(function (aModel) {
          aModel.addObserver(aObserver);
      });
  },

  removeObserver: function (aObserver) {
      this._models.forEach(function (aModel) {
          aModel.removeObserver(aObserver);
      });
  },

  notifyObservers: function (aTopic, aData) {
      this._models.forEach(function (aModel) {
          aModel.notifyObservers(aTopic, aData);
      });
  }
};

function DialogsM() {
    Model.call(this);
}
DialogsM.prototype = Util.inheritPrototype({
  alert: function (aMsg, aCallback) {
      this.notifyObservers("alert", {msg: aMsg, callback: aCallback});
  },

  confirm: function (aMsg, aCallback) {
      this.notifyObservers("confirm", {msg: aMsg, callback: aCallback});
  }
}, Model.prototype);

function DialogsVc(aDialogs) {
    ViewController.call(this);
    aDialogs.jsavc = this;
    aDialogs.addEventListener("click", this, false);
    this._dialogs = aDialogs;
}
DialogsVc.prototype = Util.inheritPrototype({
  observe: function (aSubject, aTopic, aData) {
      switch (aTopic) {
        case "alert": this._alert(aData.msg, aData.callback); break;
        case "confirm": this._confirm(aData.msg, aData.callback); break;
      }
  },

  onClick: function (aEvent) {
      var target = aEvent.target;

      var node = target;
      while (node) {
          if (node == this._dialogs) { node = null; break; }
          if (node.classList.contains("dialog")) { break; }
          node = node.parentNode;
      }
      if (!node) { return; }

      var cb;
      var param;
      if (node.classList.contains("alert")) {
          cb = node.jsacallback;
          switch (target) {
            case node.jsaok: param = true; break;
            default: return;
          }
      } else if (node.classList.contains("confirm")) {
          cb = node.jsacallback;
          switch (target) {
            case node.jsayes: param = true; break;
            case node.jsano: param = false; break;
            default: return;
          }
      } else {
          return;
      }
      node.parentNode.removeChild(node);
      if (cb) { cb(param); }
  },

  handleEvent: function (aEvent) {
      switch (aEvent.type) {
        case "click": this.onClick(aEvent); break;
      }
  },

  /*
   * div#dialogs
   *   div.dialog.alert
   *     div.box
   *       div.msg
   *         #text
   *       div.buttons
   *         button.ok
   *           #text
   */
  _alert: function(aMsg, aCallback) {
      var d = document;
      var frag = d.createDocumentFragment();

      var dialog = d.createElement("div");
      dialog.classList.add("dialog");
      dialog.classList.add("alert");
      dialog.jsacallback = aCallback ? aCallback : null; //
      frag.appendChild(dialog);
      var box = d.createElement("div");
      box.classList.add("box");
      dialog.appendChild(box);
      var msg = d.createElement("div");
      msg.classList.add("msg");
      box.appendChild(msg);
      msg.appendChild(d.createTextNode(aMsg));
      var buttons = d.createElement("div");
      buttons.classList.add("buttons");
      box.appendChild(buttons);
      var ok = d.createElement("button");
      ok.classList.add("ok");
      dialog.jsaok = ok;//
      buttons.appendChild(ok);
      ok.appendChild(d.createTextNode("OK"));

      this._dialogs.appendChild(frag);
  },

  /*
   * div#dialogs
   *   div.dialog.confirm
   *     div.box
   *       div.msg
   *         #text
   *       div.buttons
   *         button.yes
   *           #text
   *         button.no
   *           #text
   */
  _confirm: function(aMsg, aCallback) {
      var d = document;
      var frag = d.createDocumentFragment();

      var dialog = d.createElement("div");
      dialog.classList.add("dialog");
      dialog.classList.add("confirm");
      dialog.jsacallback = aCallback ? aCallback : null; //
      frag.appendChild(dialog);
      var box = d.createElement("div");
      box.classList.add("box");
      dialog.appendChild(box);
      var msg = d.createElement("div");
      msg.classList.add("msg");
      box.appendChild(msg);
      msg.appendChild(d.createTextNode(aMsg));
      var buttons = d.createElement("div");
      buttons.classList.add("buttons");
      box.appendChild(buttons);
      var yes = d.createElement("button");
      yes.classList.add("yes");
      dialog.jsayes = yes;//
      buttons.appendChild(yes);
      yes.appendChild(d.createTextNode("Yes"));
      var no = d.createElement("button");
      no.classList.add("no");
      dialog.jsano = no;//
      buttons.appendChild(no);
      no.appendChild(d.createTextNode("No"));

      this._dialogs.appendChild(frag);
  }
}, ViewController.prototype);

/*
 * div#tree
 *   div.node.jsamenu		#root node
 *     span.title
 *       #text
 *     div.children
 *       div.node.normal
 *       div.node.normal
 * @param aTree Element
 */
function TreeM(aScriptsM, aTree) {
    Model.call(this);
    this._scriptsM = aScriptsM;
    aTree.jsam = this;
    this._tree = aTree;
    this._selection = null;
    this._saveTreeStamp = void(0);
    this._saveTreeFlag = false;
    this._autoSaveTreeTId = void(0);
    this._sweepScriptsFlag = false;
    this._autoSweepScriptsTId = void(0);
}
TreeM.prototype = Util.inheritPrototype({
  _loadTree: function () {
      return new Promise((function (aResolve, aReject) {
          var key = "tree";
          chrome.storage.local.get([key], (function (aItems) {
              var le = chrome.runtime.lastError;
              if (le) {
                  console.error(le.toString());
                  var e = new Error("Could not load tree.");
                  e.name = "FailToGet";
                  aReject(e);
                  return;
              }
              var val = aItems[key];
              if (!val) {
                  val = this._createTNode("jsamenu");
                  val.title = "JsActions";
              }
              this.importData(val, false, false);
              this.select(null);
              aResolve();
          }).bind(this));
      }).bind(this));
  },

  _saveTree: function () {
      return new Promise((function (aResolve, aReject) {
          var stamp = this._createStamp();
          this._setSaveTreeStamp(stamp);
          var items = {
            "treeStamp": stamp,
            "tree": this.exportData()
          };
          chrome.storage.local.set(items, (function () {
              var le = chrome.runtime.lastError;
              if (le) {
                  console.error(le.toString());
                  var e = new Error("Could not save tree.");
                  e.name = "FailToSet";
                  aReject(e);
                  return;
              }
              aResolve();
          }).bind(this));
      }).bind(this));
  },

  _createStamp: function () {
      return new Date().valueOf();
  },

  _getSaveTreeStamp: function () {
      return this._saveTreeStamp;
  },

  _setSaveTreeStamp: function (aStamp) {
      this._saveTreeStamp = aStamp;
  },

  _getSaveTreeFlag: function () {
      return this._saveTreeFlag;
  },

  _setSaveTreeFlag: function (aFlag) {
      this._saveTreeFlag = aFlag;
  },

  _startAutoSaveTree: function () {
      this._autoSaveTreeTId = window.setInterval((function () {
          if (this._getSaveTreeFlag()) {
              this._setSaveTreeFlag(false);
              this._saveTree().then(function () {
              }).catch(function (aE) {
                  console.error(aE.toString());
              });
          }
      }).bind(this), 1000);
  },

  _stopAutoSaveTree: function () {
      window.clearInterval(this._autoSaveTreeTId);
  },

  init: function (aAutoSaveTree, aAutoSweepScripts) {
      if (typeof aAutoSaveTree == "undefined") { aAutoSaveTree = true; }
      if (typeof aAutoSweepScripts == "undefined") {
          aAutoSweepScripts = true;
      }

      chrome.storage.onChanged.addListener(this.onChanged.bind(this));
      return new Promise((function (aResolve, aReject) {
          this._loadTree().then((function () {
              if (aAutoSaveTree) { this._startAutoSaveTree(); }
              if (aAutoSweepScripts) { this._startAutoSweepScripts(); }
              aResolve();
          }).bind(this)).catch(function (aE) {
              aReject(aE);
          });
      }).bind(this));
  },

  _getTree: function () {
      return this._tree;
  },

  onChanged: function (aChanges, aAreaName) {
      if (aAreaName != "local") { return; }
      var tkey = "tree";
      if (!(tkey in aChanges)) { return; }
      var stamp = this._getSaveTreeStamp();
      if (stamp) {
          var tskey = "treeStamp";
          if ((tskey in aChanges) && aChanges[tskey].newValue == stamp) {
              this._setSaveTreeStamp(void(0));
          } else {
              var rootNode = this._getTree().firstChild;
              var append = !!(rootNode && rootNode.jsachildren);
              this.importData(aChanges[tkey].newValue, append, false);
              this._saveTree().then(function () {
              }).catch(function (aE) {
                  console.error(aE.toString());
              });
          }
          return;
      }
      this.importData(aChanges[tkey].newValue, false, false);
      this.select(null);
  },

  _createPNode: function (aParam) {
      var copy = typeof aParam == "object";
      var type = copy ? aParam.type : aParam;
      var pNode = null;
      switch (type) {
        case "jsamenu":
          pNode = {
            type: type,
            contexts: copy ? aParam.contexts.slice() :
              ["page", "selection", "link", "image", "video", "audio"],
            title: copy ? aParam.title : "New Item"
          };
          break;
        case "normal":
          pNode = {
            type: type,
            contexts: copy ? aParam.contexts.slice() :
              ["page", "selection", "link", "image", "video", "audio"],
            title: copy ? aParam.title : "New Item",
            scriptReqs: copy ? aParam.scriptReqs.slice() : [],
            scriptId: copy ? aParam.scriptId : this._scriptsM.genId()
          };
          break;
        case "separator":
          pNode = {
            type: type,
            contexts: copy ? aParam.contexts.slice() :
              ["page", "selection", "link", "image", "video", "audio"]
          };
          break;
        default:
          throw new Error("Unsupported type of node: " + type);
      }
      return pNode;
  },

  _createTNode: function (aParam) {
      var tNode = this._createPNode(aParam);
      if (tNode.type == "jsamenu") {
          tNode["children"] = [];
      }
      return tNode;
  },

  _tNodeToPNode: function (atNode) {
      return this._createPNode(atNode);
  },

  _pNodeToTNode: function (apNode) {
      return this._createTNode(apNode);
  },

  _createDNode: function (aParam) {
      var pNode = this._createPNode(aParam);
      var type = pNode.type;

      var d = document;
      var frag = d.createDocumentFragment();

      var dNode = d.createElement("div");
      dNode.classList.add("node");
      dNode.classList.add(type);
      dNode.jsanode = pNode; //
      frag.appendChild(dNode);

      var dTitle = d.createElement("span");
      dTitle.classList.add("title");
      dNode.appendChild(dTitle);
      switch (type) {
        case "jsamenu":
        case "normal":
          dTitle.textContent = pNode.title;
          break;
        case "separator":
          dTitle.textContent = "----------------";
          break;
      }
      dNode.jsatitle = dTitle; //

      switch (type) {
        case "jsamenu":
          var dChildren = d.createElement("div");
          dChildren.classList.add("children");
          dNode.appendChild(dChildren);
          dNode.jsachildren = dChildren; //
          break;
        //case "normal": break;
        //case "separator": break;
      }

      return frag;
  },

  _pNodeToDNode: function (apNode) {
      return this._createDNode(apNode);
  },

  _tNodeToDNode: function (atNode) {
      function t2dr(aModel, atNode, aParent) {
          var frag = aModel._pNodeToDNode(atNode);
          var dNode = frag.firstChild;
          aParent.appendChild(frag);

          var tNodes = atNode.children;
          if (!tNodes) { return; }

          var dChildren = dNode.jsachildren;
          for (var i = 0, l = tNodes.length; i < l; ++i) {
              t2dr(aModel, tNodes[i], dChildren);
          }
      }

      var d = document;
      var frag = d.createDocumentFragment();
      t2dr(this, atNode, frag);
      return frag;
  },

  _dNodeToTNode: function (adNode) {
      function d2tr(aModel, adNode) {
          var tNode = aModel._pNodeToTNode(adNode.jsanode);

          var dChildren = adNode.jsachildren;
          if (!dChildren) { return tNode; }

          var tchildren = tNode.children;
          var dNode = dChildren.firstChild;
          for (; dNode; dNode = dNode.nextSibling) {
              tchildren.push(d2tr(aModel, dNode));
          }

          return tNode;
      }

      return d2tr(this, adNode);
  },

  importData: function (aData, aAppend, aSave) {
      var tree = this._getTree();
      var rootNode = tree.firstChild;
      if (!rootNode || typeof aAppend == "undefined") { aAppend = false; }
      if (typeof aSave == "undefined") { aSave = true; }

      var frag = this._tNodeToDNode(aData);
      if (!aAppend) {
          frag.firstChild.classList.add("root");
      }

      if (rootNode && aAppend) {
          var rootChildren = rootNode.jsachildren;
          if (!rootChildren) {
              throw new Error("The root node type is not 'Menu'.");
          }
          rootChildren.appendChild(frag);
      } else {
          var range = document.createRange();
          range.selectNodeContents(tree);
          range.deleteContents();
          tree.appendChild(frag);
      }

      if (aSave) { this._setSaveTreeFlag(true); }
  },

  exportData: function () {
      return this._dNodeToTNode(this._getTree().firstChild);
  },

  _sweepScripts: function () {
      function tidsr(adNode, aIds) {
          var pNode = adNode.jsanode;
          switch (pNode.type) {
            //case "jsamenu": break;
            case "normal": aIds.add(pNode.scriptId); break;
            //case "separator": break;
          }

          var dChildren = adNode.jsachildren;
          if (!dChildren) { return; }

          var dNode = dChildren.firstChild;
          for (; dNode; dNode = dNode.nextSibling) {
              tidsr(dNode, aIds);
          }
      }

      return new Promise((function (aResolve, aReject) {
          var scriptsM = this._scriptsM;
          var tids = new Set();
          tidsr(this._getTree().firstChild, tids);
          var sids = scriptsM.exportIds();
          var ids = [];
          sids.forEach(function (aId) {
              if (!tids.has(aId)) { ids.push(aId); }
          });
          if (ids.length == 0) { aResolve(); return; }
          scriptsM.remove(ids, function (aE) {
              if (aE) { aReject(aE); return; }
              aResolve();
          });
      }).bind(this));
  },

  _getSweepScriptsFlag: function () {
      return this._sweepScriptsFlag;
  },

  _setSweepScriptsFlag: function (aFlag) {
      this._sweepScriptsFlag = aFlag;
  },

  _startAutoSweepScripts: function () {
      this._autoSweepScriptsTId = window.setInterval((function () {
          if (this._getSweepScriptsFlag()) {
              this._setSweepScriptsFlag(false);
              this._sweepScripts().then(function () {
              }).catch(function (aE) {
                  console.error(aE.toString());
              });
          }
      }).bind(this), 1000);
  },

  _stopAutoSweepScripts: function () {
      window.clearInterval(this._autoSweepScriptsTId);
  },

  _getSelection: function () {
      return this._selection;
  },

  _setSelection: function (adNode) {
      this._selection = adNode;
  },

  add: function (aTest) {
      var refNode = this._getSelection();
      if (!refNode) { return false; }
      if (refNode.classList.contains("root") &&
          refNode.jsanode.type != "jsamenu") { return false; }
      if (aTest) { return true; }

      var frag = this._createDNode("normal");
      var newNode = frag.firstChild;
      var pNode = refNode.jsanode;
      if (pNode.type == "jsamenu") {
          refNode.jsachildren.appendChild(frag);
      } else {
          refNode.parentNode.insertBefore(frag, refNode.nextSibling);
      }
      this._setSaveTreeFlag(true);
      this.select(newNode);
      return true;
  },

  remove: function (aTest) {
      var dNode = this._getSelection();
      if (!dNode) { return false; }
      if (dNode.classList.contains("root")) { return false; }
      if (aTest) { return true; }
      var sweep = false;
      var pNode = dNode.jsanode;
      switch (pNode.type) {
        case "jsamenu":
          if (dNode.jsachildren.firstChild) { sweep = true; }
          break;
        case "normal":
          sweep = true;
          break;
        //case "separator": break;
      }
      var selNode = dNode.nextElementSibling;
      if (!selNode) { selNode = dNode.previousElementSibling; }
      if (!selNode) { selNode = dNode.parentNode.parentNode; }
      dNode.parentNode.removeChild(dNode);
      this._setSaveTreeFlag(true);
      if (sweep) { this._setSweepScriptsFlag(true); }
      this.select(selNode);
      return true;
  },

  up: function (aTest) {
      var dNode = this._getSelection();
      if (!dNode) { return false; }
      if (dNode.classList.contains("root")) { return false; }
      var refNode = dNode.previousSibling;
      if (!refNode) { return false; }
      if (aTest) { return true; }
      dNode.parentNode.insertBefore(dNode, refNode);
      this._setSaveTreeFlag(true);
      this.select(dNode);
      return true;
  },

  down: function (aTest) {
      var dNode = this._getSelection();
      if (!dNode) { return false; }
      if (dNode.classList.contains("root")) { return false; }
      var nxdNode = dNode.nextSibling;
      if (!nxdNode) { return false; }
      if (aTest) { return true; }
      dNode.parentNode.insertBefore(dNode, nxdNode.nextSibling);
      this._setSaveTreeFlag(true);
      this.select(dNode);
      return true;
  },

  left: function (aTest) {
      var dNode = this._getSelection();
      if (!dNode) { return false; }
      if (dNode.classList.contains("root")) { return false; }
      var pdNode = dNode.parentNode.parentNode;
      if (pdNode.classList.contains("root")) { return false; }
      if (aTest) { return true; }
      pdNode.parentNode.insertBefore(dNode, pdNode.nextSibling);
      this._setSaveTreeFlag(true);
      this.select(dNode);
      return true;
  },

  right: function (aTest) {
      var dNode = this._getSelection();
      if (!dNode) { return false; }
      var pvdNode = dNode.previousSibling;
      if (!pvdNode || pvdNode.jsanode.type != "jsamenu") { return false; }
      if (aTest) { return true; }
      pvdNode.jsachildren.appendChild(dNode);
      this._setSaveTreeFlag(true);
      this.select(dNode);
      return true;
  },

  select: function (adNode) {
      var dNode = this._getSelection();
      if (adNode != dNode) {
          if (dNode) {
              dNode.classList.remove("selected");
          }
          if (adNode) {
              adNode.classList.add("selected");
          }
          this._setSelection(adNode);
      }

      this.notifyObservers("selection.changed", null);
  },

  selectionHasChildren: function () {
      var dNode = this._getSelection();
      if (!dNode) { return false; }
      return !!(dNode.jsanode.type == "jsamenu" &&
                dNode.jsachildren.firstChild);
  },

  validateType: function (aType) {
      var types = new Set();
      ["jsamenu", "normal", "separator"].
        forEach(function (aItem) { types.add(aItem); });
      if (!types.has(aType)) {
          return "A type has to be the following one of them: " +
                   "jsamenu, normal or separator";
      }
      return "";
  },

  validateContexts: function (aContexts) {
      var contexts = new Set();
      ["page", "selection", "link", "image", "video", "audio"].
        forEach(function (aItem) { contexts.add(aItem); });
      for (var i = 0, l = aContexts.length; i < l; ++i) {
          if (!contexts.has(aContexts[i])) {
              return "Contexts have to be combination of the following " +
                       "values: page, selection, link, image, video, audio";
          }
      }
      return "";
  },

  validateTitle: function (aTitle) {
      if (aTitle == "") { return "Please enter a title."; }
      if (aTitle == "." || aTitle == "..") {
          return "A title as \".\" or \"..\" is not allowed.";
      }
      if (aTitle.match(/[\/\\:\*\?\"<>|]/)) {
          return "A title cannot contain the following characters: " +
                   "\\/:*?\"<>|";
      }
      return "";
  },

  getProperties: function () {
      var dNode = this._getSelection();
      if (!dNode) { return null; }
      return this._createPNode(dNode.jsanode);
  },

  setProperties: function (aProperties) {
      var dNode = this._getSelection();
      if (!dNode) { return; }

      var sweep = false;

      var pNode = dNode.jsanode;
      var opNodes = dNode.jsaoldnodes;
      if (!opNodes) { opNodes = {}; }
      var npNode;
      if (("type" in aProperties) && aProperties.type != pNode.type) {
          if (pNode.type == "jsamenu" && dNode.jsachildren.firstChild) {
              sweep = true;
          }

          var param = opNodes[aProperties.type];
          if (!param) { param = aProperties.type; }
          npNode = this._createPNode(param);
          for (var i in npNode) {
              if (!(i in pNode)) { continue; }
              switch (i) {
                //case "type": break;
                case "contexts":
                case "scriptReqs":
                  npNode[i] = pNode[i].slice();
                  break;
                case "title":
                case "scriptId":
                  npNode[i] = pNode[i];
                  break;
              }
          }

          opNodes[pNode.type] = pNode;
      } else {
          npNode = this._createPNode(pNode);
      }

      var title, msg, e;
      for (var i in aProperties) {
          if (!(i in npNode)) {
              throw new Error("Unsupported name of property: " + i);
          }

          switch (i) {
            //case "type": break;
            case "contexts":
            case "scriptReqs":
              npNode[i] = aProperties[i].slice();
              break;
            case "title":
              title = aProperties[i];
              msg = this.validateTitle(title);
              if (msg) {
                  this.select(dNode);
                  e = new Error(msg);
                  e.name = "InvTitle";
                  throw e;
              }
              npNode[i] = title;
              break;
            case "scriptId":
              npNode[i] = aProperties[i];
              break;
          }
      }

      var frag = this._pNodeToDNode(npNode);
      var ndNode = frag.firstChild;
      ndNode.jsaoldnodes = opNodes;
      if (dNode.jsachildren && ndNode.jsachildren) {
          var range = document.createRange();
          range.selectNodeContents(dNode.jsachildren);
          ndNode.jsachildren.appendChild(range.extractContents());
      }
      if (dNode.classList.contains("root")) {
          ndNode.classList.add("root");
      }
      dNode.parentNode.insertBefore(frag, dNode);
      dNode.parentNode.removeChild(dNode);
      this._setSaveTreeFlag(true);
      if (sweep) { this._setSweepScriptsFlag(true); }
      this.select(ndNode);
  },
}, Model.prototype);

function ScriptsM() {
    Model.call(this);
    this._ids = null;
    this._saveIdsStamp = void(0);
}
ScriptsM.prototype = Util.inheritPrototype({
  _getIds: function () {
      return this._ids;
  },

  _setIds: function (aIds) {
      this._ids = aIds;
  },

  _loadIds: function () {
      return new Promise((function (aResolve, aReject) {
          var key = "scriptIds";
          chrome.storage.local.get([key], (function (aItems) {
              var le = chrome.runtime.lastError;
              if (le) {
                  console.error(le.toString());
                  var e = new Error("Could not load script IDs.");
                  e.name = "FailToGet";
                  aReject(e);
                  return;
              }
              var val = aItems[key];
              var ids = new Set();
              if (val) { val.forEach(function (aId) { ids.add(aId); }); }
              this._setIds(ids);
              aResolve();
          }).bind(this));
      }).bind(this));
  },

  _saveIds: function () {
      return new Promise((function (aResolve, aReject) {
          var vids = [];
          this._getIds().forEach(function(aId) { vids.push(aId); });
          var vstamp = this._createStamp();
          this._setSaveIdsStamp(vstamp);
          var items = {
            "scriptIdsStamp": vstamp,
            "scriptIds": vids
          };
          chrome.storage.local.set(items, (function () {
              var le = chrome.runtime.lastError;
              if (le) {
                  console.error(le.toString());
                  var e = new Error("Could not save script IDs.");
                  e.name = "FailToSet";
                  aReject(e);
                  return;
              }
              aResolve();
          }).bind(this));
      }).bind(this));
  },

  _createStamp: function () {
      return new Date().valueOf();
  },

  _getSaveIdsStamp: function () {
      return this._saveIdsStamp;
  },

  _setSaveIdsStamp: function (aStamp) {
      this._saveIdsStamp = aStamp;
  },

  init: function () {
      chrome.storage.onChanged.addListener(this.onChanged.bind(this));
      return this._loadIds();
  },

  onChanged: function (aChanges, aAreaName) {
      if (aAreaName != "local") { return; }
      var skey = "scriptIds";
      if (!(skey in aChanges)) { return; }
      var ids;
      var stamp = this._getSaveIdsStamp();
      if (stamp) {
          var sskey = "scriptIdsStamp";
          if ((sskey in aChanges) && aChanges[sskey].newValue == stamp) {
              this._setSaveIdsStamp(void(0));
          } else {
              ids = new Set();
              var oids = this._getIds();
              oids.forEach(function (aId) { ids.add(aId); });
              aChanges[skey].newValue.forEach(function (aId) { ids.add(aId); });
              this._setIds(ids);
              this._saveIds().then(function () {
              }).catch(function (aE) {
                  console.error(aE.toString());
              });
          }
          return;
      }
      ids = new Set();
      aChanges[skey].newValue.forEach(function (aId) { ids.add(aId); });
      this._setIds(ids);
  },

  exportIds: function () {
      var ids = new Set();
      this._getIds().forEach(function (aId) { ids.add(aId); });
      return ids;
  },

  _genGuid: function() {
      var str = "ABCDEF0123456789";
      var id = "{";
      for (var i = 0, m = str.length - 1; i < 32; ++i) {
          id += str[Math.floor(Math.random() * m)];
          switch (i) {
            case 7:
            case 11:
            case 15:
            case 19:
              id += "-";
              break;
          }
      }
      id += "}"
      return id;
  },

  genId: function() {
      var ids = this._getIds();
      var id;
      do {
          id = this._genGuid();
      } while (ids.has(id));
      return id;
  },

  validateId: function (aId) {
      if (!aId.match(/^\{[0-9A-F]{8}(?:-[0-9A-F]{4}){3}-[0-9A-F]{12}\}$/)) {
          return "An ID have to be GUID form with braces.";
      }
      return "";
  },

  get: function (aIds, aCallback) {
      if (typeof aIds == "string") { aIds = [aIds]; }
      var keys = [];
      aIds.forEach(function (aId) { keys.push("script." + aId); });
      chrome.storage.local.get(keys, function (aItems) {
          var le = chrome.runtime.lastError;
          if (le) {
              console.error(le.toString());
              var e = new Error("Could not load scripts.");
              e.name = "FailToGet";
              aCallback({}, e);
              return;
          }
          var items = {};
          for (var key in aItems) {
              items[key.substring(7)] = aItems[key];
          }
          aCallback(items);
      });
  },

  set: function (aItems, aCallback) {
      var ids = new Set();
      var items = {};
      var msg, e;
      for (var id in aItems) {
          msg = this.validateId(id);
          if (msg) {
              e = new Error(msg);
              e.name = "InvID";
              aCallback(e);
              return;
          }
          ids.add(id);
          items["script." + id] = aItems[id];
      }

      var oids = this._getIds();
      oids.forEach(function (aId) { ids.add(aId); });
      new Promise((function (aResolve, aReject) {
          if (ids.size != oids.size) {
              var val = [];
              ids.forEach(function(aId) { val.push(aId); });
              this._setIds(ids);
              this._saveIds().then(function () {
                  aResolve();
              }).catch(function (aE) {
                  aReject(aE);
              });
          } else {
              aResolve();
          }
      }).bind(this)).then(function () {
          chrome.storage.local.set(items, function () {
              var le = chrome.runtime.lastError;
              if (le) {
                  console.error(le.toString());
                  var e = new Error("Could not save scripts.");
                  e.name = "FailToSet";
                  aCallback(e);
                  return;
              }
              aCallback();
          });
      }).catch(function (aE) {
          aCallback(aE);
      });
  },

  remove: function (aIds, aCallback) {
      if (typeof aIds == "string") { aIds = [aIds]; }
      var keys = [];
      aIds.forEach(function (aId) { keys.push("script." + aId); });
      chrome.storage.local.remove(keys, (function () {
          var le = chrome.runtime.lastError;
          if (le) {
              console.error(le.toString());
              var e = new Error("Could not delete scripts.");
              e.name = "FailToSet";
              aCallback(e);
              return;
          }

          var oids = this._getIds();
          var ids = new Set();
          oids.forEach(function (aId) { ids.add(aId); });
          aIds.forEach(function (aId) { ids.delete(aId); });
          if (ids.size != oids.size) {
              this._setIds(ids);
              this._saveIds().then(function () {
                  aCallback();
              }).catch(function (aE) {
                  aCallback(aE);
              });
          } else {
              aCallback();
          }
      }).bind(this));
  }
}, Model.prototype);

function ToolBarVc(aToolBar, abImportSamples,
                   abImport, afImportBack, abExport, aaExportBack) {
    ViewController.call(this);
    aToolBar.jsavc = this;
    aToolBar.addEventListener("click", this, false);
    aToolBar.addEventListener("change", this, false);
    this._toolBar = aToolBar;
    this._bImportSamples = abImportSamples;
    this._bImport = abImport;
    this._fImportBack = afImportBack;
    this._bExport = abExport;
    this._aExportBack = aaExportBack;
}
ToolBarVc.prototype = Util.inheritPrototype({
  _exportData: function (aCallback) {
      var treeM = this._model.tree;
      var scriptsM = this._model.scripts;

      var tree = treeM.exportData();
      var ids = [];
      scriptsM.exportIds().forEach(function (aId) { ids.push(aId); });
      scriptsM.get(ids, function (aItems, aError) {
          if (aError) { aCallback({}, aError); return; }
          var data = {
              version: 1,
              tree: tree,
              script: aItems
          };
          aCallback(data);
      });
  },

  _importData: function (aData, aCallback) {
      var treeM = this._model.tree;
      var scriptsM = this._model.scripts;
      var e;

      if (typeof aData != "object") {
          e = new Error("Invalid format: /");
          e.name = "FailToImport";
          aCallback(e);
          return;
      }

      function t2tr(aTreeM, aScriptsM, aNode, aSegs, aTable) {
          var msg, type, contexts, title, children, scriptReqs, scriptId, e;

          if (typeof aNode != "object") {
              e = new Error("Invalid format: /" + aSegs.join("/"));
              e.name = "FailToImport";
              throw e;
          }

          type = aNode.type;
          if (typeof type != "string") {
              e = new Error("Invalid format: /" + aSegs.join("/") + "/type");
              e.name = "FailToImport";
              throw e;
          }
          msg = aTreeM.validateType(type);
          if (msg) {
              e = new Error("Invalid format: /" + aSegs.join("/") + "/type");
              e.name = "FailToImport";
              throw e;
          }

          contexts = aNode.contexts;
          if (!(contexts instanceof Array)) {
              e = new Error("Invalid format: /" + aSegs.join("/") +
                              "/contexts");
              e.name = "FailToImport";
              throw e;
          }
          for (var i = 0, l = contexts.length; i < l; ++i) {
              if (typeof contexts[i] != "string") {
                  e = new Error("Invalid format: /" + aSegs.join("/") +
                                  "/contexts");
                  e.name = "FailToImport";
                  throw e;
              }
          }
          msg = aTreeM.validateContexts(contexts);
          if (msg) {
              e = new Error("Invalid format: /" + aSegs.join("/") +
                              "/contexts");
              e.name = "FailToImport";
              throw e;
          }

          switch (type) {
            case "jsamenu":
              title = aNode.title;
              if (typeof title != "string") {
                  e = new Error("Invalid format: /" + aSegs.join("/") +
                                  "/title");
                  e.name = "FailToImport";
                  throw e;
              }
              msg = aTreeM.validateTitle(title);
              if (msg) {
                  e = new Error("Invalid format: /" + aSegs.join("/") +
                                  "/title");
                  e.name = "FailToImport";
                  throw e;
              }

              children = aNode.children;
              if (!(children instanceof Array)) {
                  e = new Error("Invalid format: /" + aSegs.join("/") +
                                  "/children");
                  e.name = "FailToImport";
                  throw e;
              }
              break;
            case "normal":
              title = aNode.title;
              if (typeof title != "string") {
                  e = new Error("Invalid format: /" + aSegs.join("/") +
                                  "/title");
                  e.name = "FailToImport";
                  throw e;
              }
              msg = aTreeM.validateTitle(title);
              if (msg) {
                  e = new Error("Invalid format: /" + aSegs.join("/") +
                                  "/title");
                  e.name = "FailToImport";
                  throw e;
              }

              scriptReqs = aNode.scriptReqs;
              if (!(scriptReqs instanceof Array)) {
                  e = new Error("Invalid format: /" + aSegs.join("/") +
                                  "/scriptReqs");
                  e.name = "FailToImport";
                  throw e;
              }
              for (var i = 0, l = scriptReqs.length; i < l; ++i) {
                  if (typeof scriptReqs[i] != "string") {
                      e = new Error("Invalid format: /" + aSegs.join("/") +
                                      "/scriptReqs");
                      e.name = "FailToImport";
                      throw e;
                  }
              }

              scriptId = aNode.scriptId;
              if (typeof scriptId != "string") {
                  e = new Error("Invalid format: /" + aSegs.join("/") +
                                  "/scriptId");
                  e.name = "FailToImport";
                  throw e;
              }
              msg = aScriptsM.validateId(scriptId);
              if (msg) {
                  e = new Error("Invalid format: /" + aSegs.join("/") +
                                  "/scriptId");
                  e.name = "FailToImport";
                  throw e;
              }
              break;
            //case "separator": break;
          }

          var nnode;
          switch (type) {
            case "jsamenu":
              var nchildren = [];
              for (var i = 0, l = children.length; i < l; ++i) {
                  nchildren.push(t2tr(aTreeM,
                                      aScriptsM,
                                      children[i],
                                      aSegs.concat("children[" + i + "]"),
                                      aTable));
              }
              nnode = {
                type: type,
                contexts: contexts.slice(),
                title: title,
                children: nchildren
              };
              break;
            case "normal":
              nnode = {
                type: type,
                contexts: contexts.slice(),
                title: title,
                scriptReqs: scriptReqs.slice(),
                scriptId: scriptId
              };
              if (!(scriptId in aTable)) { aTable[scriptId] = []; }
              aTable[scriptId].push(nnode);
              break;
            case "separator":
              nnode = {
                type: type,
                contexts: contexts.slice()
              };
              break;
          }
          return nnode
      }

      var tree = aData.tree;
      var table = {}; //key: scriptId, value: array of normal node
      var ntree;
      try {
          ntree = t2tr(treeM, scriptsM, tree, ["tree"], table);
      } catch (aE) {
          aCallback(aE);
          return;
      }

      var script = aData.script;
      if (typeof script != "object") {
          e = new Error("Invalid format: /script");
          e.name = "FailToImport";
          aCallback(e);
          return;
      }
      var nscript = {};
      var nodes, code, nid;
      for (var id in table) {
          if (!(id in script)) { continue; }

          code = script[id];
          if (typeof code != "string") {
              e = new Error("Invalid format: /script[\"" + id + "\"]");
              e.name = "FailToImport";
              aCallback(e);
              return;
          }

          nid = scriptsM.genId();
          nodes = table[id];
          for (var i = 0, l = nodes.length; i < l; ++i) {
              nodes[i].scriptId = nid;
          }
          nscript[nid] = code;
      }

      treeM.importData(ntree, true, true);
      scriptsM.set(nscript, function (aError) {
          if (aError) {
              e = new Error(aError.message);
              e.name = "FailToImport";
              aCallback(e);
              return;
          }
          aCallback();
      });
  },

  _createFileReader: function () {
      return new FileReader();
  },

  _importFile: function (aFile) {
      return new Promise((function (aResolve, aReject) {
          var cb = (function (aEvent) {
              switch (aEvent.type) {
                case "error":
                  aReject(new Error("Could not read file."));
                  return;
                  break;
                case "load":
                  var data = null;
                  try {
                      data = JSON.parse(aEvent.target.result);
                  } catch (e) {
                      aReject(new Error("Could not parse file."));
                      return;
                  }
                  this._importData(data, function (aError) {
                      if (aError) { aReject(aError); return; }
                      aResolve();
                  });
                  break;
              }
          }).bind(this);
          var reader = this._createFileReader();
          reader.addEventListener("error", cb, false);
          reader.addEventListener("load", cb, false);
          reader.readAsText(aFile);
      }).bind(this));
  },

  onChange: function (aEvent) {
      var target = aEvent.target;
      var dialogsM = this._model.dialogs;

      switch (target) {
        case this._fImportBack:
          var files = target.files;
          if (!files || files.length < 1) {
              dialogsM.alert("There is no file to read.");
              return;
          }
          this._importFile(files[0]).then(function () {
          }).catch(function (aE) {
              dialogsM.alert(aE.message);
          });
          break;
      }
  },

  _createXHR: function () {
      return new XMLHttpRequest();
  },

  _importSamples: function () {
      return new Promise((function (aResolve, aReject) {
          var url = chrome.extension.getURL("samples.json");
          var request = this._createXHR();
          var cb = (function (aEvent) {
              switch (aEvent.type) {
                case "error":
                  aReject(new Error("Could not read file."));
                  return;
                  break;
                case "load":
                  var data = null;
                  try {
                      data = JSON.parse(aEvent.target.responseText);
                  } catch (e) {
                      aReject(new Error("Could not parse file."));
                      return;
                  }
                  this._importData(data, function (aError) {
                      if (aError) { aReject(aError); return; }
                      aResolve();
                  });
                  break;
              }
          }).bind(this);
          request.addEventListener("error", cb, false);
          request.addEventListener("load", cb, false);
          request.open("GET", url);
          request.send();
      }).bind(this));
  },

  onClick: function (aEvent) {
      var dialogsM = this._model.dialogs;

      switch (aEvent.target) {
        case this._bImportSamples:
          this._importSamples().then(function () {
          }).catch(function (aE) {
              dialogsM.alert(aE.message);
          });
          break;
        case this._bImport:
          this._fImportBack.click();
          break;
        case this._bExport:
          this._exportData((function (aData, aError) {
              if (aError) { dialogsM.alert(aError.message); return; }

              var blob = new Blob([JSON.stringify(aData)],
                                  {type: "application/json"});
              this._aExportBack.href = window.URL.createObjectURL(blob);
              this._aExportBack.click();
          }).bind(this));
          break;
      }
  },

  handleEvent: function (aEvent) {
      switch (aEvent.type) {
        case "click": this.onClick(aEvent); break;
        case "change": this.onChange(aEvent); break;
      }
  }
}, ViewController.prototype);

function EditBarVc(aEditBar, abAdd, abRemove, abUp, abDown, abLeft, abRight) {
    ViewController.call(this);
    aEditBar.jsavc = this;
    aEditBar.addEventListener("click", this, false);
    this._editBar = aEditBar;
    abAdd.disabled = true;
    this._bAdd = abAdd;
    abRemove.disabled = true;
    this._bRemove = abRemove;
    abUp.disabled = true;
    this._bUp = abUp;
    abDown.disabled = true;
    this._bDown = abDown;
    abLeft.disabled = true;
    this._bLeft = abLeft;
    abRight.disabled = true;
    this._bRight = abRight;
}
EditBarVc.prototype = Util.inheritPrototype({
  observe: function (aSubject, aTopic, aData) {
      switch (aTopic) {
        case "selection.changed":
          var treeM = this._model.tree;
          this._bAdd.disabled = !treeM.add(true);
          this._bRemove.disabled = !treeM.remove(true);
          this._bUp.disabled = !treeM.up(true);
          this._bDown.disabled = !treeM.down(true);
          this._bLeft.disabled = !treeM.left(true);
          this._bRight.disabled = !treeM.right(true);
          break;
      }
  },

  onClick: function (aEvent) {
      var dialogsM = this._model.dialogs;
      var treeM = this._model.tree;

      var node = aEvent.target;
      switch (node) {
        case this._bAdd: treeM.add(); break;
        case this._bRemove:
          if (treeM.selectionHasChildren()) {
              dialogsM.confirm("Specified node is not empty. " +
                                 "Are you sure you want to continue?",
                               function (aResult) {
                                   if (!aResult) { return; }
                                   treeM.remove();
                               });
          } else {
              treeM.remove();
          }
          break;
        case this._bUp: treeM.up(); break;
        case this._bDown: treeM.down(); break;
        case this._bLeft: treeM.left(); break;
        case this._bRight: treeM.right(); break;
      }
  },

  handleEvent: function (aEvent) {
      switch (aEvent.type) {
        case "click": this.onClick(aEvent); break;
      }
  }
}, ViewController.prototype);

function TreeVc(aTree) {
    ViewController.call(this);
    aTree.jsavc = this;
    aTree.addEventListener("click", this, false);
    this._tree = aTree;
}
TreeVc.prototype = Util.inheritPrototype({
  onClick: function (aEvent) {
      var node = aEvent.target;
      while (node) {
          if (node == this._tree) { node = null; break; }
          if (node.classList.contains("node")) { break; }
          node = node.parentNode;
      }
      this._model.select(node);
  },

  handleEvent: function (aEvent) {
      switch (aEvent.type) {
        case "click": this.onClick(aEvent); break;
      }
  }
}, ViewController.prototype);

function NotebookVc(aTabs) {
    aTabs.jsavc = this;
    aTabs.addEventListener("click", this, false);
    this._tabs = aTabs;

    var map = new Map();
    var mtabs = [];
    var mpages = [];
    var mtab;
    var mpage;
    for (var i = 1, l = arguments.length; i < l; ++i) {
        mtab = arguments[i];
        mpage = arguments[++i];
        map.set(mtab, mpage);
        mtabs.push(mtab);
        mpages.push(mpage);
    }
    this._map = map;
    this._mtabs = mtabs;
    this._mpages = mpages;

    this._selection = null; //mtab
}
NotebookVc.prototype = {
  _getSelection: function () {
      return this._selection;
  },

  _setSelection: function (aNode) {
      this._selection = aNode;
  },

  _select: function (aNode) {
      if (!aNode) { return; }
      var skey = this._getSelection();
      if (skey) {
          var sval = this._map.get(skey);
          skey.classList.remove("selected");
          sval.classList.remove("selected");
      } else {
          this._map.forEach(function (aKey, aVal) {
              aKey.classList.remove("selected");
              aVal.classList.remove("selected");
          });
      }
      var key = aNode;
      var val = this._map.get(key);
      key.classList.add("selected");
      val.classList.add("selected");
      this._setSelection(key);
  },

  onClick: function (aEvent) {
      var node = aEvent.target;
      while (node) {
          if (node == this._tabs) { node = null; break; }
          if (node.classList.contains("tab")) { break; }
          node = node.parentNode;
      }
      this._select(node);
  },

  handleEvent: function (aEvent) {
      switch (aEvent.type) {
        case "click": this.onClick(aEvent); break;
      }
  }
};

function PropertiesVc(aProperties, asType, atTitle,
                      acPageCtx, acSelectionCtx, acLinkCtx,
                      acImageCtx, acVideoCtx, acAudioCtx) {
    ViewController.call(this);
    aProperties.jsavc = this;
    aProperties.addEventListener("change", this, false);
    this._properties = aProperties;
    asType.disabled = true;
    this._sType = asType;
    atTitle.disabled = true;
    this._tTitle = atTitle;
    acPageCtx.disabled = true;
    this._cPageCtx = acPageCtx;
    acSelectionCtx.disabled = true;
    this._cSelectionCtx = acSelectionCtx;
    acLinkCtx.disabled = true;
    this._cLinkCtx = acLinkCtx;
    acImageCtx.disabled = true;
    this._cImageCtx = acImageCtx;
    acVideoCtx.disabled = true;
    this._cVideoCtx = acVideoCtx;
    acAudioCtx.disabled = true;
    this._cAudioCtx = acAudioCtx;
}
PropertiesVc.prototype = Util.inheritPrototype({
  observe: function (aSubject, aTopic, aData) {
      switch (aTopic) {
        case "selection.changed":
          var treeM = this._model.tree;
          var props = treeM.getProperties();

          var typedis = !(props && ("type" in props));
          var titldis = !(props && ("title" in props));
          var ctxsdis = !(props && ("contexts" in props));

          this._sType.disabled = typedis;
          this._tTitle.disabled = titldis;
          this._cPageCtx.disabled = ctxsdis;
          this._cSelectionCtx.disabled = ctxsdis;
          this._cLinkCtx.disabled = ctxsdis;
          this._cImageCtx.disabled = ctxsdis;
          this._cVideoCtx.disabled = ctxsdis;
          this._cAudioCtx.disabled = ctxsdis;

          this._sType.value = typedis ? "" : props.type;
          this._tTitle.value = titldis ? "" : props.title;
          var cset = null;
          if (!ctxsdis) {
              cset = new Set();
              props.contexts.forEach(function (aCtx) { cset.add(aCtx); });
          }
          this._cPageCtx.checked = !cset ? false : cset.has("page");
          this._cSelectionCtx.checked = !cset ? false : cset.has("selection");
          this._cLinkCtx.checked = !cset ? false : cset.has("link");
          this._cImageCtx.checked = !cset ? false : cset.has("image");
          this._cVideoCtx.checked = !cset ? false : cset.has("video");
          this._cAudioCtx.checked = !cset ? false : cset.has("audio");
          break;
      }
  },

  onChange: function (aEvent) {
      var dialogsM = this._model.dialogs;
      var treeM = this._model.tree;
      switch (aEvent.target) {
        case this._sType:
          if (treeM.selectionHasChildren()) {
              dialogsM.
                confirm("Specified node is not empty. " +
                          "Are you sure you want to continue?",
                        (function (aResult) {
                            if (!aResult) { return; }
                            treeM.setProperties({type: this._sType.value});
                        }).bind(this));
          } else {
              treeM.setProperties({type: this._sType.value});
          }
          break;
        case this._tTitle:
          try {
              treeM.setProperties({title: this._tTitle.value});
          } catch (aE) {
              dialogsM.alert(aE.message);
          }
          break;
        case this._cPageCtx:
        case this._cSelectionCtx:
        case this._cLinkCtx:
        case this._cImageCtx:
        case this._cVideoCtx:
        case this._cAudioCtx:
          var ctxs = [];
          if (this._cPageCtx.checked) { ctxs.push("page"); }
          if (this._cSelectionCtx.checked) { ctxs.push("selection"); }
          if (this._cLinkCtx.checked) { ctxs.push("link"); }
          if (this._cImageCtx.checked) { ctxs.push("image"); }
          if (this._cVideoCtx.checked) { ctxs.push("video"); }
          if (this._cAudioCtx.checked) { ctxs.push("audio"); }
          treeM.setProperties({contexts: ctxs});
          break;
      }
  },

  handleEvent: function (aEvent) {
      switch (aEvent.type) {
        case "change": this.onChange(aEvent); break;
      }
  }
}, ViewController.prototype);

function ScriptVc(aScript, aRequires, aCode) {
    ViewController.call(this);
    aScript.jsavc = this;
    aScript.addEventListener("change", this, false);
    this._script = aScript;
    aRequires.disabled = true;
    this._requires = aRequires;
    aCode.disabled = true;
    this._code = aCode;
}
ScriptVc.prototype = Util.inheritPrototype({
  observe: function (aSubject, aTopic, aData) {
      switch (aTopic) {
        case "selection.changed":
          var dialogsM = this._model.dialogs;
          var treeM = this._model.tree;
          var props = treeM.getProperties();

          var reqsdis = !(props && ("scriptReqs" in props));
          var codedis = !(props && ("scriptId" in props));

          this._requires.disabled = reqsdis;
          this._code.disabled = codedis;

          this._requires.value = reqsdis ? "" : props.scriptReqs.join(":");
          if (!codedis) {
              var scriptsM = this._model.scripts;
              scriptsM.get(props.scriptId, (function (aItems, aError) {
                  if (aError) { dialogsM.alert(aError.message); }
                  var val = aItems[props.scriptId];
                  this._code.value = val ? val : "";
              }).bind(this));
          } else {
              this._code.value = "";
          }
          break;
      }
  },

  onChange: function (aEvent) {
      var dialogsM = this._model.dialogs;
      var treeM = this._model.tree;
      var scriptsM = this._model.scripts;
      switch (aEvent.target) {
        case this._requires:
          var reqs = this._requires.value.split(/:/).
            filter(function (aItem, aIndex, aArray) { return aItem != ""; });
          treeM.setProperties({scriptReqs: reqs});
          break;
        case this._code:
          var props = treeM.getProperties();
          if (props && ("scriptId" in props)) {
              var items = {};
              items[props.scriptId] = this._code.value;
              scriptsM.set(items, function (aError) {
                  if (aError) { dialogsM.alert(aError.message); }
              });
          }
          break;
      }
  },

  handleEvent: function (aEvent) {
      switch (aEvent.type) {
        case "change": this.onChange(aEvent); break;
      }
  }
}, ViewController.prototype);
