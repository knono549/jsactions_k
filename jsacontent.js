/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict';

var scriptExecutor = {
  _target: null,
  _unsafeNames: null,

  init: function () {
      this._target = null;
      this._unsafeNames = ["browser", "chrome", "scriptExecutor"];
      document.addEventListener("mousedown", this, true);
      document.addEventListener("keydown", this, true);
      chrome.runtime.onMessage.addListener(this.onMessage.bind(this));
  },

  getTarget: function () {
      return this._target;
  },

  addUnsafeNames: function (aNames) {
      if (typeof aNames == "string") { aNames = [aNames]; }
      this._unsafeNames = this._unsafeNames.concat(aNames);
  },

  getUnsafeNames: function () {
      return this._unsafeNames.slice(0);
  },

  onMousedown: function (aEvent) {
      if (aEvent.button != 2) { return; }

      this._target = aEvent.target;
  },

  onKeydown: function (aEvent) {
      if (aEvent.code != "ContextMenu") { return; }

      this._target = aEvent.target;
  },

  handleEvent: function (aEvent) {
      switch (aEvent.type) {
        case "mousedown": this.onMousedown(aEvent); break;
        case "keydown": this.onKeydown(aEvent); break;
      }
  },

  execScript: function (aScript, aCScript, aUnsafeNames) {
      var _jsaCScript = aCScript;
      // eval the script safely as far as possible
      return eval("(function () {" +
                  "    var " + aUnsafeNames.join(", ") + ";" +
                  "    var aScript, aCScript, aUnsafeNames;" +
                  aScript +
                  "}).call({});");
  },

  _createJsaCScript: function (aEvent, aContext, aInfo) {
      return new JsaCScript(aEvent, aContext, aInfo);
  },

  onMessage: function (aMessage, aSender, aSendResponse) {
      if (!document.hasFocus() ||
          ('contentDocument' in document.activeElement)) { return; }

      var event = aMessage.event;
      var context = aMessage.context;
      context.target = this.getTarget();
      var node = aMessage.node;
      var paths = aMessage.paths;
      var scripts = aMessage.scripts;

      var msg;
      var path;
      var script;

      for (var i = 0, l = paths.length; i < l; ++i) {
          path = paths[i];
          script = scripts[i];
          if (!script) {
              msg = paths[l - 1] +
                ': LoadError: Could not load the script: ' + path;
              console.error(msg);
              return;
          }
      }

      path = paths[paths.length - 1];
      script = scripts[scripts.length - 1];
      var cScript =
        this._createJsaCScript(event,
                               context,
                               {
                                 scriptTitle: node.title,
                                 scriptReqs: node.scriptReqs.slice(),
                                 scriptSource: script,
                                 scriptPath: path
                               });
      try {
          this.execScript(scripts.join("\n"), cScript, this._unsafeNames);
      } catch (e) {
          msg = path;
          if ('lineNumber' in e) { msg += ': line ' + e.lineNumber; }
          msg += ': ' + e.name + ': ' + e.message;
          console.error(msg);
          return;
      }
  }
};
