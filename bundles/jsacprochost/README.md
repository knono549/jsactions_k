INTRODUCTION
============

The native messaging host for JSActions.


REQUIREMENTS
------------

* Python 3.6.3
  https://www.python.org/


INSTALLATION
------------

## UNIX

    $ cd <repository root>/bundles/jsacprochost/
    $ python install.py <destination directory>

    $ cd ~/.config/JSActions/
    $ vi jsacprochost.cfg

    {"whitelist":["cmd 1","cmd 2"]}

## Mac OS X

    $ cd <repository root>/bundles/jsacprochost/
    $ python install.py <destination directory>

    $ cd "~/Library/Application Support/JSActions"
    $ vi jsacprochost.cfg

    {"whitelist":["cmd 1","cmd 2"]}

## Windows

    > cd <repository root>\bundles\jsacprochost\
    > python install.py <destination directory>

    > %HOMEDRIVE%
    > cd "%APPDATA%\JSActions"
    > vi jsacprochost.cfg

    {"whitelist":["cmd 1","cmd 2"]}
