#!/usr/bin/env python3 -u
# -*- coding: utf-8 -*-
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import argparse
import json
import logging
import os
import os.path
import shutil
import stat
import subprocess
import sys

g_logger = None

def main(argv):
    global g_logger

    logging.basicConfig(stream=sys.stderr, level=logging.INFO)
    g_logger = logging.getLogger(__name__)

    parser = argparse.ArgumentParser(description="Install jsacprochost.py")
    parser.add_argument("destpath", metavar="dest", type=str,
                        help="a destination path")
    args = parser.parse_args(argv[1:])

    execpath = os.path.join(args.destpath, "jsacprochost.py")

    srcpath = os.path.dirname(argv[0])
    try:
        print("Copying jsacprochost.py ... ", end="")
        shutil.copyfile(os.path.join(srcpath, "jsacprochost.py"), execpath)
        print("done", end="")
    except (shutil.SameFileError, shutil.Error) as e:
        g_logger.error(str(e))
        return 1
    except (OSError, IOError) as e:
        g_logger.error(str(e))
        return 1
    finally:
        print("")

    if os.name == "nt":
        print("Writing jsacprochost.bat ... ", end="")
        execpath = os.path.join(args.destpath, "jsacprochost.bat")
        batcode = \
"""@echo off

set args=

:ARGS_BEGIN
if \"%~1\" == \"\" goto ARGS_END
set args=%args%%1 
shift
goto ARGS_BEGIN
:ARGS_END

py -3 -u \"%~p0\jsacprochost.py\" %args%
"""
        batfile = None
        try:
            batfile = open(execpath, "w", newline="\r\n")
            batfile.write(batcode)
            print("done", end="")
        except IOError as e:
            g_logger.error(str(e))
            return 1
        finally:
            try: batfile.close()
            except: pass
            print("")
    elif os.name == "posix":
        print("Changing the mode of jsacprochost.py ... ", end="")
        os.chmod(execpath,
                 stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR | \
                   stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH | \
                   stat.S_IXOTH) #0755
        print("done")

    manifpath = None
    if os.name == "nt":
        manifpath = os.path.join(args.destpath, "jsacprochost.json")
    elif os.name == "posix" and sys.platform.startswith("darwin"):
        manifpath = \
          os.path.join(os.path.expanduser("~/Library/Application Support"),
                       "NativeMessagingHosts",
                       "jsacprochost.json")
    elif os.name == "posix":
        manifpath = \
          os.path.join(os.path.expanduser("~/.mozilla"),
                       "native-messaging-hosts",
                       "jsacprochost.json")

    if not manifpath:
        g_logger.error(
            "Could not determine the location of the manifest file.")
        return 1

    print("Writing jsacprochost.manifest ... ", end="")
    manifdata = {
      "name": "jsacprochost",
      "description": "Native messaging host for " \
                       "JsaCScript.Runtime#callProcess",
      "path": os.path.abspath(execpath),
      "type": "stdio",
      "allowed_extensions": ["{6FCC1566-47E3-47ca-AAD7-AA3320FF4717}"]
    }
    maniffile = None
    try:
        os.makedirs(os.path.dirname(manifpath), exist_ok=True)
        maniffile = open(manifpath, "w", newline="\n")
        maniffile.write(json.dumps(manifdata))
        print("done", end="")
    except (OSError, IOError) as e:
        g_logger.error(str(e))
        return 1
    finally:
        try: maniffile.close()
        except: pass
        print("")

    if os.name == "nt":
        print("Adding the registry entries ... ", end="")
        regkey = \
          "HKEY_CURRENT_USER\\SOFTWARE\\Mozilla\\NativeMessagingHosts\\" \
          "jsacprochost"
        popen = subprocess.Popen(["reg", "add", regkey, "/ve", "/d",
                                  os.path.abspath(manifpath), "/f" ],
                                 stdout=subprocess.DEVNULL,
                                 stderr=subprocess.DEVNULL)
        popen.wait()
        if popen.returncode > 0:
            g_logger.error("An error occured during adding registry entries.")
            return 1
        print("done")

    return 0

# execute
if __name__ == "__main__":
    sys.exit(main(sys.argv))
