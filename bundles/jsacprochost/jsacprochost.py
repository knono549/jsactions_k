#!/usr/bin/python3 -u
# -*- coding: utf-8 -*-
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import io
import json
import logging
import os
import os.path
import subprocess
import struct
import sys

g_logger = None
g_whitelist = set()

class JsaCProcHostError(Exception):
    pass

def load_cfg(argv):
    cfpath = None
    if os.name == "nt":
        appdata = os.getenv("APPDATA", None)
        if appdata:
            cfpath = os.path.join(
                appdata, "JSActions",
                os.path.splitext(os.path.basename(argv[0]))[0] + ".cfg")
    elif os.name == "posix" and sys.platform.startswith("darwin"):
        cfpath = os.path.join(
            os.path.expanduser("~/Library/Application Support/JSActions"),
            os.path.splitext(os.path.basename(argv[0]))[0] + ".cfg")
    elif os.name == "posix":
        cfpath = os.path.join(
            os.path.expanduser("~/.config/JSActions"),
            os.path.splitext(os.path.basename(argv[0]))[0] + ".cfg")

    if not cfpath:
        g_logger.info("Could not determine the location of the config file.")
        return

    cfile = None
    try:
        cfile = open(cfpath, encoding="utf-8")
    except OSError as e:
        g_logger.info(str(e))
        return

    cfg = None
    try:
        cfg = json.loads(cfile.read())
    except (IOError, json.decoder.JSONDecodeError) as e:
        g_logger.error(str(e))
        raise JsaCProcHostError("Could not parse the config file.")
    finally:
        try: cfile.close()
        except: pass

    if "whitelist" in cfg:
        whitelist = cfg["whitelist"]
        if not isinstance(whitelist, list):
            raise JsaCProcHostError("Invalid format: /whitelist")
        for i,v in enumerate(whitelist):
            if not isinstance(v, str):
                raise JsaCProcHostError(
                    "Invalid format: /whitelist/[%d]" % i)
            g_whitelist.add(v)

def get_next_message():
    stdin = sys.stdin.buffer
    try:
        buf = stdin.read(4)
        if len(buf) < 4:
            if len(buf) > 0:
                g_logger.debug("Could read only %db (%db) from stdin." %
                               (len(buf), 4))
            return None
        blen = struct.unpack("@I", buf)[0]
        buf = stdin.read(blen)
        if len(buf) < blen:
            g_logger.debug("Could read only %db (%db) from stdin." %
                             (len(buf), blen))
            return None
        return json.loads(buf.decode("utf-8"))
    except IOError as e:
        g_logger.error(str(e))
        raise JsaCProcHostError("Could not get a next message.")

def send_response(msg):
    stdout = sys.stdout.buffer
    try:
        buf = json.dumps(msg).encode("utf-8")
        blen = len(buf)
        stdout.write(struct.pack("@I", blen))
        stdout.write(buf)
        stdout.flush()
        return
    except IOError as e:
        g_logger.error(str(e))
        raise JsaCProcHostError("Could not send a response.")

def process_message(msg):
    file = msg["file"] if "file" in msg else None

    if not isinstance(file, str) or not file in g_whitelist:
        raise JsaCProcHostError("Specified file isn't white-listed: " + file)

    params = msg["params"] if "params" in msg else []
    directory = msg["directory"] if "directory" in msg else None
    communicate = msg["communicate"] if "communicate" in msg else False
    input = msg["input"] if "input" in msg else None
    inencoding = \
      msg["inencoding"] if "inencoding" in msg else sys.__stdin__.encoding
    outencoding = \
      msg["outencoding"] if "outencoding" in msg else sys.__stdout__.encoding
    errencoding = None
    if "errencoding" in msg:
        errencoding = msg["errencoding"]
    elif "outencoding" in msg:
        errencoding = msg["outencoding"]
    else:
        errencoding = sys.__stderr__.encoding

    rmsg = {"result": {}}
    try:
        args = {}
        args["args"] = [file] + params
        if directory: args["cwd"] = directory
        if communicate:
            args["stdin"] = subprocess.PIPE if input else None
            args["stdout"] = subprocess.PIPE
            args["stderr"] = subprocess.PIPE
        else:
            args["close_fds"] = True
            if os.name == "nt":
                args["creationflags"] = 0x01000000 #CREATE_BREAKAWAY_FROM_JOB
            if os.name == "posix":
                args["preexec_fn"] = os.setsid
        popen = subprocess.Popen(**args) 
        rmsg["result"]["pid"] = popen.pid
        if communicate:
            (out, err) = \
              popen.communicate(input.encode(inencoding) if input else None)
            rmsg["result"]["returncode"] = popen.returncode
            rmsg["result"]["output"] = out.decode(outencoding)
            rmsg["result"]["error"] = err.decode(errencoding)
    except (OSError, TypeError, ValueError) as e:
        g_logger.error(str(e))
        raise JsaCProcHostError("Could not call a process.")

    return rmsg

def main(argv):
    global g_logger

    sys.stdin = io.TextIOWrapper(sys.stdin.detach(),
                                 line_buffering=sys.stdin.line_buffering,
                                 encoding="utf-8")
    sys.stdout = io.TextIOWrapper(sys.stdout.detach(),
                                  line_buffering=sys.stdout.line_buffering,
                                  encoding="utf-8")
    sys.stderr = io.TextIOWrapper(sys.stderr.detach(),
                                  line_buffering=sys.stderr.line_buffering,
                                  encoding="utf-8")

    logging.basicConfig(stream=sys.stderr, level=logging.INFO)
    g_logger = logging.getLogger(__name__)

    try:
        load_cfg(argv) #g_whitelist
    except JsaCProcHostError as e:
        g_logger.error(str(e))
        return 1

    try:
        msg = get_next_message()
        while msg:
            send_response(process_message(msg))
            msg = get_next_message()
    except JsaCProcHostError as e:
        g_logger.error(str(e))
        try:
            send_response({
                            "error": {
                              "name": "JsaCProcHostError",
                              "message": str(e)
                            }
                          })
        except JsaCProcHostError as e:
            g_logger.error(str(e))
        return 1

    return 0

# execute
if __name__ == "__main__":
    sys.exit(main(sys.argv))
