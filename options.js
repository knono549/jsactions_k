/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict';

function init() {
    var d = document;

    var dialogsM = new DialogsM();
    var dialogs = d.getElementById("Dialogs");
    var dialogsVc = new DialogsVc(dialogs);
    dialogsVc.setModel(dialogsM);
    var scriptsM = new ScriptsM();
    var tree = d.getElementById("Tree");
    var treeM = new TreeM(scriptsM, tree);
    var proxyM1 = new ProxyM(dialogsM, treeM, scriptsM);
    var proxyM2 = new ProxyM(dialogsM, treeM, null);
    Promise.all([scriptsM.init(), treeM.init()]).
      then(function () {
          var toolBar = d.getElementById("ToolBar");
          var bImportSamples = d.getElementById("ImportSamples");
          var bImport = d.getElementById("Import");
          var fImportBack = d.getElementById("ImportBack");
          var bExport = d.getElementById("Export");
          var aExportBack = d.getElementById("ExportBack");
          var toolBarVc =
            new ToolBarVc(toolBar, bImportSamples,
                          bImport, fImportBack, bExport, aExportBack);
          toolBarVc.setModel(proxyM1);

          var editBar = d.getElementById("EditBar");
          var bAdd = d.getElementById("Add");
          var bRemove = d.getElementById("Remove");
          var bUp = d.getElementById("Up");
          var bDown = d.getElementById("Down");
          var bLeft = d.getElementById("Left");
          var bRight = d.getElementById("Right");
          var editBarVc =
            new EditBarVc(editBar, bAdd, bRemove, bUp, bDown, bLeft, bRight);
          editBarVc.setModel(proxyM2);

          var treeVc = new TreeVc(tree);
          treeVc.setModel(treeM);

          var tabs = d.getElementById("Tabs");
          var propsTab = d.getElementById("PropertiesTab");
          var propsPage = d.getElementById("PropertiesPage");
          var scriptTab = d.getElementById("ScriptTab");
          var scriptPage = d.getElementById("ScriptPage");
          var notebookVc =
            new NotebookVc(tabs, propsTab, propsPage, scriptTab, scriptPage);

          var props = d.getElementById("Properties");
          var sType = d.getElementById("Type");
          var tTitle = d.getElementById("Title");
          var cPageCtx = d.getElementById("PageContext");
          var cSelectionCtx = d.getElementById("SelectionContext");
          var cLinkCtx = d.getElementById("LinkContext");
          var cImageCtx = d.getElementById("ImageContext");
          var cVideoCtx = d.getElementById("VideoContext");
          var cAudioCtx = d.getElementById("AudioContext");
          var propsVc =
            new PropertiesVc(props, sType, tTitle,
                             cPageCtx, cSelectionCtx, cLinkCtx,
                             cImageCtx, cVideoCtx, cAudioCtx);
          propsVc.setModel(proxyM2);

          var script = d.getElementById("Script");
          var requires = d.getElementById("Requires");
          var code = d.getElementById("Code");
          var scriptVc = new ScriptVc(script, requires, code);
          scriptVc.setModel(proxyM1);
      }).catch(function (aError) {
          dialogsM.alert(aError.message);
      });
}

// execute
init();
